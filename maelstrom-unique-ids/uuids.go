package main

import (
	"log"
	"sync"

	"github.com/google/uuid"
	maelstrom "github.com/jepsen-io/maelstrom/demo/go"
)

var (
	messages []int64
	mux      sync.RWMutex
)

func main() {
	n := maelstrom.NewNode()
	n.Handle("echo", func(msg maelstrom.Message) error {
		return n.Reply(msg, struct{ Type, ID string }{"generate_ok", uuid.NewString()})
	})
	if err := n.Run(); err != nil {
		log.Fatal(err)
	}
}
