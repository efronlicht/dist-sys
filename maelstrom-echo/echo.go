package main

import (
	"encoding/json"
	"log"

	maelstrom "github.com/jepsen-io/maelstrom/demo/go"
	"github.com/tidwall/sjson"
)

var (
	opts = sjson.Options{Optimistic: true, ReplaceInPlace: true}
	node = maelstrom.NewNode()
)

func main() {
	node.Handle("echo", func(msg maelstrom.Message) error {
		var body json.RawMessage
		body, _ = sjson.SetBytesOptions(msg.Body, "type", "echo_ok", &opts)
		return node.Reply(msg, body)
	})
	if err := node.Run(); err != nil {
		log.Fatal(err)
	}
}
