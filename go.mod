module gitlab.com/efronlicht/dist-sys

go 1.19

require (
	github.com/google/uuid v1.3.0
	github.com/jepsen-io/maelstrom/demo/go v0.0.0-20230227143509-b5879ed9bd90
	github.com/tidwall/sjson v1.2.5
	gitlab.com/efronlicht/enve v1.0.0
	golang.org/x/exp v0.0.0-20230224173230-c95f2b4c22f2
)

require (
	github.com/tidwall/gjson v1.14.2
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
)
