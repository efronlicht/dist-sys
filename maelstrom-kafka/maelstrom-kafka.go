package main

import (
	"encoding/json"
	"sync/atomic"

	maelstrom "github.com/jepsen-io/maelstrom/demo/go"
	"golang.org/x/exp/slices"
)

var (
	node = maelstrom.NewNode()

	id atomic.Int64
)

func must[T any](t T, err error) T {
	if err != nil {
		panic(err)
	}
	return t
}
func jsonAs[T any](src any) (dst T) { json.Unmarshal(must(json.Marshal(src)), &dst); return dst }

func main() {
	node.Handle("commit_offsets", commitOffsets)
	node.Handle("list_committed_offsets", listCommittedOffsets)
	node.Handle("poll", poll)
	node.Handle("send", send)
	if err := node.Run(); err != nil {
		panic(err)
	}
}

func send(msg maelstrom.Message) error {
	body := fromJSON[struct {
		Key string `json:"key"`
		Msg int    `json:"msg"`
	}](msg.Body)
	offset := newOffset(body.Key)
	appendEntry(body.Key, offset, body.Msg)
	return node.Reply(msg, struct {
		Type   string `json:"type"`
		Offset int    `json:"offset"`
		MsgID  int    `json:"msg_id"`
	}{Type: "send_ok", Offset: offset, MsgID: newID()})
}

func commitOffsets(msg maelstrom.Message) error {
	body := fromJSON[struct{ Offsets map[string]int }](msg.Body)
	needsUpdate := make(map[string]bool, len(body.Offsets))
	{
		cache.Lock()
		for k, want := range body.Offsets {
			if e := cache.m[k]; len(e) == 0 || e[len(e)-1][0] < want {
				needsUpdate[k] = true
			}
		}
		cache.Unlock()
	}
	for k := range needsUpdate {
		getEntries(k)
	}
	return node.Reply(msg, struct {
		Type  string `json:"type"`
		MsgID int    `json:"msg_id"`
	}{
		Type:  "commit_offsets_ok",
		MsgID: newID(),
	})
}

func cmpEntry(a, b [2]int) int {
	switch {
	case a[0] < b[0]:
		return -1
	case a[0] > b[0]:
		return 1
	default:
		return 0
	}
}

func listCommittedOffsets(msg maelstrom.Message) error {
	body := fromJSON[struct {
		Type string   `json:"type"`
		Keys []string `json:"keys"`
	}](msg.Body)
	offsets := make(map[string]int, len(body.Keys))
	cache.Lock()
	for _, k := range body.Keys {
		offsets[k] = lastOffset(cache.m[k])
	}
	cache.Unlock()
	return node.Reply(msg, struct {
		MsgID   int            `json:"msg_id"`
		Type    string         `json:"type"`
		Offsets map[string]int `json:"offsets"`
	}{
		Type: "list_committed_offsets_ok", Offsets: offsets, MsgID: newID(),
	})
}

func poll(msg maelstrom.Message) error {
	body := fromJSON[struct {
		Type    string         `json:"type"`
		Offsets map[string]int `json:"offsets"`
		Msg     int            `json:"msg"`
	}](msg.Body)
	msgs := make(map[string]logEntry, len(body.Offsets))

	// read from cache, holding the lock as little time as possible.
	cache.Lock()
	for k := range body.Offsets {
		msgs[k] = cache.m[k]
	}
	cache.Unlock()
	for k, e := range msgs {
		// check if cached entries are current.
		start := body.Offsets[k]
		if lastOffset(e) < start {
			e = getEntries(k) // refresh the cache.
		}
		// trim the cached entry to start at the correct offset, omitting empty entries.
		if i, _ := slices.BinarySearchFunc(e, [2]int{start, 0}, cmpEntry); i > 0 {
			msgs[k] = e[i:]
		}

	}
	return node.Reply(msg, struct {
		MsgID int                 `json:"msg_id"`
		Type  string              `json:"type"`
		Msgs  map[string]logEntry `json:"msgs"`
	}{Type: "poll_ok", Msgs: msgs, MsgID: newID()})
}
func newID() int { return int(id.Add(1)) }

func fromJSON[T any](b json.RawMessage) (t T) {
	if err := json.Unmarshal(b, &t); err != nil {
		panic(err)
	}
	return t
}

func lastOffset(e logEntry) int {
	if len(e) == 0 {
		return 0
	}
	return e[len(e)-1][0]
}
