package main

import (
	crand "crypto/rand"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"

	maelstrom "github.com/jepsen-io/maelstrom/demo/go"
	"gitlab.com/efronlicht/enve"
)

var node = maelstrom.NewNode()
var bitset [1024]uint64

func setbits(i int, b uint64) {
	for {
		old := atomic.LoadUint64(&bitset[i])
		new := old | b
		if old == new {
			return
		}
		if atomic.CompareAndSwapUint64(&bitset[i], old, new) {
			return
		}
	}
}

func mergeOffsets(offsets []int16, bits []uint64) {
	if len(offsets) != len(bits) {
		panic(fmt.Errorf("expected len(offsets) == len(bits), but had %d=%d", len(offsets), len(bits)))
	}
	for j := range offsets {
		i, b := int(offsets[j]), bits[j]
		setbits(i, b)
	}
}

func getOffsets() (offsets []int16, bits []uint64) {
	offsets, bits = make([]int16, 64), make([]uint64, 64)
	for i, b := range bitset {
		if b != 0 {
			offsets = append(offsets, int16(i))
			bits = append(bits, b)
		}
	}
	return offsets, bits
}

func main() {
	node.Handle("broadcast", broadcast)
	node.Handle("last_seen", lastSeen)
	node.Handle("read", read)
	node.Handle("topology", topology)
	if err := node.Run(); err != nil {
		log.Fatal(err)
	}
}

var topologyOnce sync.Once

// initialize sendLastSeen on the first receipt of topology.
func topology(m maelstrom.Message) error {
	body := fromJSON[struct {
		Type     string              `json:"type,omitempty"`
		MsgID    int                 `json:"msg_id,omitempty"`
		Topology map[string][]string `json:"topology,omitempty"`
	}](m.Body)

	neighbors := body.Topology[node.ID()]
	topologyOnce.Do(func() {
		go sendLastSeen(neighbors)
	})

	return node.Reply(m, maelstrom.MessageBody{
		Type:      "topology_ok",
		MsgID:     newID(),
		InReplyTo: body.MsgID,
	})
}

type LastSeen struct {
	Type    string   `json:"type,omitempty"`
	MsgID   int      `json:"msg_id,omitempty"`
	Offsets []int16  `json:"offsets,omitempty"`
	Bits    []uint64 `json:"bits,omitempty"`
}

// send all the messages we've seen to randomly chosen neighbors every so often.
// to avoid network congestion, we send
func sendLastSeen(neighbors []string) {
	rng := newRNG()
	skipBelow := enve.FloatOr("SKIP_SYNC_BELOW", 0.8)
	for range time.Tick(enve.DurationOr("SYNC_EVERY", 75*time.Millisecond)) {
		offsets, bits := getOffsets()
		for _, id := range neighbors {
			if rng.Float64() < skipBelow {
				continue
			}
			_ = node.Send(id, LastSeen{
				Type:    "last_seen",
				MsgID:   newID(),
				Offsets: offsets,
				Bits:    bits,
			})
		}
	}
}

// when we receive a last_seen,
// add all messages received to our own map.
func lastSeen(m maelstrom.Message) error {
	body := fromJSON[LastSeen](m.Body)
	mergeOffsets(body.Offsets, body.Bits)
	return nil
}

func broadcast(m maelstrom.Message) error {
	body := fromJSON[struct {
		Type    string `json:"type,omitempty"`
		MsgID   int    `json:"msg_id,omitempty"`
		Message int    `json:"message,omitempty"`
	}](m.Body)
	i, bit := body.Message/64, 1<<(body.Message%64)
	setbits(i, uint64(bit))

	return node.Reply(m, maelstrom.MessageBody{
		Type:      "broadcast_ok",
		MsgID:     newID(),
		InReplyTo: body.MsgID,
	})
}

// send a copy of all known keys out.
func read(m maelstrom.Message) error {
	var messages []int
	for i, b := range bitset {
		if b == 0 {
			continue
		}
		for j := 0; j < 64; j++ {
			if b&(1<<j) == 0 {
				continue
			}
			messages = append(messages, i*64+j)

		}
	}
	return node.Reply(m, struct {
		Type      string `json:"type,omitempty"`
		MsgID     int    `json:"msg_id,omitempty"`
		Messages  []int  `json:"messages"`
		InReplyTo int    `json:"in_reply_to,omitempty"`
	}{
		Type:      "read_ok",
		MsgID:     newID(),
		InReplyTo: fromJSON[maelstrom.MessageBody](m.Body).MsgID,
		Messages:  messages,
	})
}

func fromJSON[T any](body json.RawMessage) (t T) {
	if err := json.Unmarshal(body, &t); err != nil {
		log.Printf("failed to unmarshal a %T: %v", t, err)
	}
	return t
}

func newRNG() *rand.Rand {
	b := make([]byte, 8)
	_, _ = crand.Read(b)
	return rand.New(rand.NewSource(int64(binary.LittleEndian.Uint64(b))))
}

var _id int64

func newID() int { return int(atomic.AddInt64(&_id, 1)) }
