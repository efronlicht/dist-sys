use io::{BufRead, BufReader};
use std::io;
use dist_sys_rust::*;
fn main() -> io::Result<()> {
    let mut stdout = io::stdout().lock();
    let mut stdin = BufReader::new(io::stdin().lock());
    let line = &mut String::new();

    _ = stdin.read_line(line).unwrap();
    let node_id = init(&mut stdout, line)?;

    loop {
        line.clear();
        stdin.read_line(line)?;
        let msg: RawMsg = serde_json::from_str(line).unwrap();
    
        raw_reply(&mut stdout, msg, &node_id, "echo_ok")?;
    }
}