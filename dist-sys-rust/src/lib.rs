use serde::{Deserialize, Serialize};
use std::io;
use std::io::Write;
use std::os::linux::raw;
use std::sync::atomic;
use std::borrow::{Cow, Cow::Borrowed};

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq)]
pub struct Init {
    pub node_id: String,
    pub node_ids: Vec<String>,
}

#[derive(Deserialize, Serialize)]
pub struct RawMsg<'a> {
    #[serde(borrow)]
    pub src: Cow<'a, str>,
    #[serde(borrow, rename="dest")]
    pub dst: Cow<'a, str>,
    pub body:  RawBody<'a>,
}
#[derive(Deserialize, Serialize)]
pub struct RawBody<'a> {
    #[serde(borrow, rename = "type")]
    pub type_: Cow<'a, str>,
    #[serde(rename = "msg_id")]
    pub id: usize,
    pub in_reply_to: Option<usize>,
    #[serde(flatten, borrow)]
    pub fields: &'a serde_json::value::RawValue,
}

pub fn raw_reply<'a>(
    mut w: impl Write,
    mut msg: RawMsg<'a>,
    node_id: &'a str,
    type_: &'a str,
) -> std::io::Result<()>{
    msg.body.type_ = Borrowed(type_);
    msg.dst = msg.src;
    msg.src = Borrowed(node_id);
    serde_json::to_writer(&mut w, &msg).unwrap();
    w.write(&[b'\n'])?;
    w.flush()
}

pub fn reply<T, B: Serialize>(
    mut w: impl Write,
    msg: &Msg<T>,
    node_id: &str,
    type_: &str,
    fields: B,
) -> std::io::Result<()> {
    let reply = Msg {
        src: Borrowed(node_id),
        dst: Borrowed(&msg.src),
        body: Body {
            type_: Borrowed(type_),
            id: next_id(),
            in_reply_to: Some(msg.body.id),
            fields,
        },
    };
    serde_json::to_writer(&mut w, &reply).unwrap();
    w.write(&[b'\n'])?;
    w.flush()
}

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq)]
pub struct Body<'a, T> {
    #[serde(borrow, rename = "type")]
    pub type_: Cow<'a, str>,
    #[serde(rename = "msg_id")]
    pub id: usize,
    pub in_reply_to: Option<usize>,
    #[serde(flatten)]
    pub fields: T,
}

pub fn next_id() -> usize {
    static mut COUNT: atomic::AtomicUsize = atomic::AtomicUsize::new(0);
    unsafe { COUNT.fetch_add(1, atomic::Ordering::AcqRel) }
}
pub fn init(w: impl Write, s: &str) -> io::Result<String> {
    let msg: Msg<Init> = serde_json::from_str(s).unwrap();
    reply(w, &msg, &msg.body.fields.node_id, "init_ok", ())?;
    Ok(msg.body.fields.node_id)
}
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq)]
pub struct Msg<'a, T> { 
    #[serde(borrow)]
    pub src: Cow<'a, str>,
    #[serde(borrow, rename = "dest")]
    pub dst: Cow<'a, str>,
    pub body: Body<'a, T>,
}
#[test]
fn test_raw_from_json() {
    let raw_msg: RawMsg  = serde_json::from_str(r#"{"src":"foo","dest":"bar","body":{"type":"init","msg_id":1,"node_id":"n3","node_ids":["n1","n2","n3"]}}"#).unwrap();
    assert_eq!(raw_msg.body.fields.to_string(), r#"{"type":"init","msg_id":1,"node_id":"n3","node_ids":["n1","n2","n3"]}"#);
}
#[test]
fn test_from_json() {
    let init: Msg<Init> = serde_json::from_str(r#"{"src":"foo","dest":"bar","body":{"type":"init","msg_id":1,"node_id":"n3","node_ids":["n1","n2","n3"]}}"#).unwrap();
    assert_eq!(init.body.fields.node_id, "n3");
    assert_eq!(init.body.fields.node_ids, ["n1", "n2", "n3"]);
}
