test-echo: 
	go install ./maelstrom-echo
	./maelstrom test -w echo --bin ~/go/bin/maelstrom-echo --node-count 1 --time-limit 10

test-counter: 
	go install ./maelstrom-counter
	./maelstrom test -w g-counter --bin ~/go/bin/maelstrom-counter --node-count 3 --rate 100 --time-limit 20 --nemesis partition

test-unique-ids: 
	go install ./maelstrom-unique-ids
	./maelstrom test -w unique-ids \
		--bin ~/go/bin/maelstrom-unique-ids \
		--time-limit 30 \
		--rate 1000 \
		--node-count 3 \
		--availability total \
		--nemesis partition

install-broadcast:
	go install ./maelstrom-broadcast

test-broadcast: test-broadcast1 test-broadcast2 test-broadcast3 test-broadcast4 test-broadcast5
test-broadcast1: install-broadcast
	./maelstrom test -w broadcast --bin ~/go/bin/maelstrom-broadcast --node-count 1 --time-limit 20 --rate 10
test-broadcast2: install-broadcast
	./maelstrom test -w broadcast --bin ~/go/bin/maelstrom-broadcast --node-count 5 --time-limit 10 --rate 10
test-broadcast3: install-broadcast
	./maelstrom test -w broadcast --topology total --bin ~/go/bin/maelstrom-broadcast --node-count 5 --time-limit 20 --rate 10 --nemesis partition
test-broadcast4: install-broadcast
	./maelstrom test -w broadcast --topology total --bin ~/go/bin/maelstrom-broadcast --node-count 25 --time-limit 20 --rate 100 --latency 100 
test-broadcast5: install-broadcast
	go install ./maelstrom-broadcast
	./maelstrom test -w broadcast --topology total --bin ~/go/bin/maelstrom-broadcast --node-count 25 --time-limit 20 --rate 100 --latency 100 

install-kafka:
	go install ./maelstrom-kafka
test-kafka-a: install-kafka
	./maelstrom test -w kafka --bin ~/go/bin/maelstrom-kafka --node-count 1 --concurrency 2n --time-limit 20 --rate 1000
test-kafka-b: install-kafka
	./maelstrom test -w kafka --bin ~/go/bin/maelstrom-kafka --node-count 2 --concurrency 2n --time-limit 20 --rate 1000
test-kafka-c: install-kafka
	./maelstrom test -w kafka --bin ~/go/bin/maelstrom-kafka --node-count 2 --concurrency 2n --time-limit 20 --rate 1000