package main

import (
	crand "crypto/rand"
	"encoding/binary"
	"encoding/json"
	"log"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"

	maelstrom "github.com/jepsen-io/maelstrom/demo/go"
	"gitlab.com/efronlicht/enve"
)

var (
	node = maelstrom.NewNode()

	mux  sync.Mutex // guards seen
	seen = make(map[int]bool)
)

func main() {
	node.Handle("broadcast", broadcast)
	node.Handle("last_seen", lastSeen)
	node.Handle("read", read)
	node.Handle("topology", topology)
	if err := node.Run(); err != nil {
		log.Fatal(err)
	}
}

var topologyOnce sync.Once

// initialize sendLastSeen on the first receipt of topology.
func topology(m maelstrom.Message) error {
	body := fromJSON[struct {
		Type     string              `json:"type,omitempty"`
		MsgID    int                 `json:"msg_id,omitempty"`
		Topology map[string][]string `json:"topology,omitempty"`
	}](m.Body)

	neighbors := body.Topology[node.ID()]
	topologyOnce.Do(func() {
		go sendLastSeen(neighbors)
	})

	return node.Reply(m, maelstrom.MessageBody{
		Type:      "topology_ok",
		MsgID:     newID(),
		InReplyTo: body.MsgID,
	})
}

// send all the messages we've seen to randomly chosen neighbors every so often.
// to avoid network congestion, we send
func sendLastSeen(neighbors []string) {
	rng := newRNG()
	var prev []int
	skipBelow := enve.FloatOr("SKIP_SYNC_BELOW", 0.8)
	for range time.Tick(enve.DurationOr("SYNC_EVERY", 75*time.Millisecond)) {
		mux.Lock()
		if len(prev) == len(seen) {
			mux.Unlock()
			continue
		}
		prev = prev[:0]
		for k := range seen {
			prev = append(prev, k)
		}
		mux.Unlock()
		for _, id := range neighbors {
			if rng.Float64() < skipBelow {
				continue
			}
			_ = node.Send(id, struct {
				Type     string
				MsgID    int
				Messages []int
			}{
				Type:     "last_seen",
				MsgID:    newID(),
				Messages: prev,
			})
		}
	}
}

// when we receive a last_seen,
// add all messages received to our own map.
func lastSeen(m maelstrom.Message) error {
	body := fromJSON[struct {
		Type     string `json:"type"`
		MsgID    int    `json:"msg_id"`
		Messages []int  `json:"messages"`
	}](m.Body)

	mux.Lock()
	for _, k := range body.Messages {
		seen[k] = true
	}
	mux.Unlock()
	return nil
}

func broadcast(m maelstrom.Message) error {
	body := fromJSON[struct {
		Type    string `json:"type,omitempty"`
		MsgID   int    `json:"msg_id,omitempty"`
		Message int    `json:"message,omitempty"`
	}](m.Body)
	go func() {
		mux.Lock()
		seen[body.Message] = true
		mux.Unlock()
	}()

	return node.Reply(m, maelstrom.MessageBody{
		Type:      "broadcast_ok",
		MsgID:     newID(),
		InReplyTo: body.MsgID,
	})
}

// send a copy of all known keys out.
func read(m maelstrom.Message) error {
	body := fromJSON[maelstrom.MessageBody](m.Body)
	mux.Lock()
	out := make([]int, 0, len(seen))
	for k := range seen {
		out = append(out, k)
	}
	mux.Unlock()
	return node.Reply(m, struct {
		Type      string `json:"type,omitempty"`
		MsgID     int    `json:"msg_id,omitempty"`
		Messages  []int  `json:"messages"`
		InReplyTo int    `json:"in_reply_to,omitempty"`
	}{
		Type:      "read_ok",
		MsgID:     newID(),
		InReplyTo: body.MsgID,
		Messages:  out,
	})
}

func fromJSON[T any](body json.RawMessage) (t T) {
	if err := json.Unmarshal(body, &t); err != nil {
		log.Printf("failed to unmarshal a %T: %v", t, err)
	}
	return t
}

func newRNG() *rand.Rand {
	b := make([]byte, 8)
	_, _ = crand.Read(b)
	return rand.New(rand.NewSource(int64(binary.LittleEndian.Uint64(b))))
}

var _id int64

func newID() int { return int(atomic.AddInt64(&_id, 1)) }
