package main

import (
	"context"
	"sync"
	"time"

	maelstrom "github.com/jepsen-io/maelstrom/demo/go"
	"golang.org/x/exp/slices"
)

type logEntry = [][2]int

var (
	seq = maelstrom.NewSeqKV(node)
	lin = maelstrom.NewLinKV(node)
)

var cache = struct { // TODO: because the set of keys only grows, and keys are disjoint, we could use a sync.Map here with a per-key mutex.
	sync.Mutex
	m map[string]logEntry
}{m: make(map[string]logEntry)}

func insert(old, new logEntry, entry [2]int) logEntry {
	i, ok := slices.BinarySearchFunc(old, entry, cmpEntry)
	if ok { // already exists
		panic("this should never happen")
	}
	new = new[:0]
	new = append(new, old[:i]...)
	new = append(new, entry)
	new = append(new, old[i:]...)
	return new
}

func newOffset(key string) int {
	for {
		ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
		old, err := seq.ReadInt(ctx, key)
		cancel()
		if err, ok := err.(*maelstrom.RPCError); ok && err.Code == maelstrom.KeyDoesNotExist {
			old = 0
		} else if err != nil {
			continue
		}
		const createIfNotExists = true
		ctx, cancel = context.WithTimeout(context.Background(), 100*time.Millisecond)
		new := old + 1
		err = seq.CompareAndSwap(ctx, key, old, new, createIfNotExists)
		cancel()
		if err == nil {
			return new
		}
	}
}

func appendEntry(key string, offset, msg int) {
	// fast path: we try and CAS with our cached entry.
	cache.Lock()
	old := cache.m[key]
	cache.Unlock()

	entry := [2]int{offset, msg}
	var new [][2]int
	for {
		new := insert(old, new, entry)
		ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
		const createIfNotExists = true
		err := lin.CompareAndSwap(ctx, key, old, new, createIfNotExists)
		cancel()
		if err == nil {
			return
		}
		// CAS failed: read the most recent entry and try again.
		old = getEntries(key)
	}
}

// get entries & refresh cache.
func getEntries(key string) (entries logEntry) {
	for {
		ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
		raw, err := lin.Read(ctx, key)
		cancel()
		if err, ok := err.(*maelstrom.RPCError); ok && err.Code == maelstrom.KeyDoesNotExist {
			return nil
		}
		if err == nil {
			entries = jsonAs[logEntry](raw)
			cache.Lock()
			cache.m[key] = entries
			cache.Unlock()
			return entries
		}
	}
}
