package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"sync/atomic"
	"time"

	maelstrom "github.com/jepsen-io/maelstrom/demo/go"
	"github.com/tidwall/gjson"
)

var (
	node  = maelstrom.NewNode()
	store = maelstrom.NewSeqKV(node)
)

var count int64

func main() {
	node.Handle("add", func(msg maelstrom.Message) error {
		delta := gjson.GetBytes(msg.Body, "delta").Int()
		add(delta)
		return node.Reply(msg, json.RawMessage(`{"type": "add_ok"}`))
	})
	node.Handle("read", func(msg maelstrom.Message) error {
		readBarrier()
		return node.Reply(msg, struct {
			Type  string `json:"type"`
			Value int    `json:"value"`
		}{
			Type:  "read_ok",
			Value: read(),
		})
	})
	if err := node.Run(); err != nil {
		log.Fatal(err)
	}
}

func read() int {
	for {
		ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
		n, err := store.ReadInt(ctx, "count")
		cancel()
		if err == nil {
			return n
		}
		if err, ok := err.(*maelstrom.RPCError); ok && err.Code == maelstrom.KeyDoesNotExist {
			return 0
		}
	}
}

func interfaceAs[T any](a []any) []T {
	out := make([]T, len(a))
	for i := range a {
		out[i] = a[i].(T)
	}
	return out
}

// write a unique value to the store, forcing later reads from this client to 'happen-after'.
// (we could write a unique KEY, but that would mean taking up indefinite amounts of memory as the number of keys grows...)
func readBarrier() {
	val := fmt.Sprintf("%s_%d", node.ID(), atomic.AddInt64(&count, 1))
	for {
		ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
		err := store.Write(ctx, "read_barrier", val)
		cancel()
		if err == nil {
			return
		}
	}
}

// repeatedly read, then attempt compare-and-swap until we succeed.
// this is a distributed spinlock.
func add(delta int64) int {
	const key = "count"

	for {
		old := read()
		new := old + int(delta)
		const createIfNotExists = true
		ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
		err := store.CompareAndSwap(ctx, key, old, new, createIfNotExists)
		cancel()
		if err == nil {
			return new
		}
	}
}
